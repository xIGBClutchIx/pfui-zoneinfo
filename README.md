This addon is an external module for [pfUI](https://gitlab.com/shagu/pfUI) addon.

## Screenshots
![image](https://i.clutchy.me/spiQ4Qwc.png)

## Description
Addon adds additional info to the zone header text. The info is recommended levels and instances. Based off of Cartographer.

## Installation
**This addon will not function without [pfUI](https://gitlab.com/shagu/pfUI) installed**
1. Download **[Latest Version](https://gitlab.com/xIGBClutchIx/pfui-zoneinfo/-/archive/master/pfui-zoneinfo-master.zip)**
2. Unpack the Zip file
3. Rename the folder to "pfui-zoneinfo"
4. Copy "pfUI-zoneinfo" into Wow-Directory\Interface\AddOns
5. Restart WoW
