## Interface: 11200
## Title: |cff33ffccpf|cffffffffUI - zoneinfo
## Author: Clutch
## Notes: External module for |cff33ffccpf|cffffffffUI that shows you zone info.
## Version: 1.0
## Dependencies: pfUI

# libs
libtourist.lua

# main
pfUI-zoneinfo.lua
