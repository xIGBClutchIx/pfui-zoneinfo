-- load pfUI environment
setfenv(1, pfUI:GetEnvironment())

pfUI.tourist = {}

local playerLevel = 1
local _,race = UnitRace("player")
local isHorde = (race == "Orc" or race == "Troll" or race == "Tauren" or race == "Scourge")
local isWestern = GetLocale() == "enUS" or GetLocale() == "deDE" or GetLocale() == "frFR" or GetLocale() == "esES"
local math_mod = math.fmod or math.mod

local Kalimdor, Eastern_Kingdoms = GetMapContinents()

local X_Y_ZEPPELIN = "%s/%s Zeppelin"
local X_Y_BOAT = "%s/%s Boat"
local X_Y_PORTAL = "%s/%s Portal"

local recZones = {}
local recInstances = {}
local lows = setmetatable({}, {__index = function() return 0 end})
local highs = setmetatable({}, getmetatable(lows))
local continents = {}
local instances = {}
local paths = {}
local types = {}
local groupSizes = {}
local factions = {}

local cost = {}

local function PLAYER_LEVEL_UP()
  playerLevel = UnitLevel("player")
  for k in pairs(recZones) do
    recZones[k] = nil
  end
  for k in pairs(recInstances) do
    recInstances[k] = nil
  end
  for k in pairs(cost) do
    cost[k] = nil
  end
  for zone in pairs(lows) do
    if not pfUI.tourist.IsHostile(zone) then
      local low, high = pfUI.tourist.GetLevel(zone)
      if types[zone] == "Zone" then
        if low <= playerLevel and playerLevel <= high then
          recZones[zone] = true
        end
      elseif types[zone] == "Battleground" then
        local playerLevel = playerLevel
        if zone == "Alterac Valley" then
          playerLevel = playerLevel - 1
        end
        if playerLevel >= low and (playerLevel == MAX_PLAYER_LEVEL or math_mod(playerLevel, 10) >= 6) then
          recInstances[zone] = true
        end
      elseif types[zone] == "Instance" then
        if low <= playerLevel and playerLevel <= high then
          recInstances[zone] = true
        end
      end
    end
  end
end

function pfUI.tourist.GetLevel(zone)
  if types[zone] == "Battleground" then
    if zone == "Alterac Valley" then
      if playerLevel <= 60 then
        return 51, 60
      else
        return 61, 70
      end
    elseif playerLevel >= MAX_PLAYER_LEVEL then
      return MAX_PLAYER_LEVEL, MAX_PLAYER_LEVEL
    elseif playerLevel >= 60 then
      return 60, 69
    elseif playerLevel >= 50 then
      return 50, 59
    elseif playerLevel >= 40 then
      return 40, 49
    elseif playerLevel >= 30 then
      return 30, 39
    elseif playerLevel >= 20 or zone == "Arathi Basin" then
      return 20, 29
    else
      return 10, 19
    end
  end
  return lows[zone], highs[zone]
end

function pfUI.tourist.GetLevelColor(zone)
  if types[zone] == "Battleground" then
    if (playerLevel < 51 and zone == "Alterac Valley") or (playerLevel < 20 and zone == "Arathi Basin") or (playerLevel < 10 and zone == "Warsong Gulch") then
      return 1, 0, 0
    end
    local playerLevel = playerLevel
    if zone == "Alterac Valley" then
      playerLevel = playerLevel - 1
    end
    if playerLevel == MAX_PLAYER_LEVEL then
      return 1, 1, 0
    end
    playerLevel = math_mod(playerLevel, 10)
    if playerLevel <= 5 then
      return 1, playerLevel / 10, 0
    elseif playerLevel <= 7 then
      return 1, (playerLevel - 3) / 4, 0
    else
      return (9 - playerLevel) / 2, 1, 0
    end
  end
  local low, high = lows[zone], highs[zone]
  
  if low <= 0 and high <= 0 then
    -- City
    return 1, 1, 1
  elseif playerLevel == low and playerLevel == high then
    return 1, 1, 0
  elseif playerLevel <= low - 3 then
    return 1, 0, 0
  elseif playerLevel <= low then
    return 1, (playerLevel - low - 3) / -6, 0
  elseif playerLevel <= (low + high) / 2 then
    return 1, (playerLevel - low) / (high - low) + 0.5, 0
  elseif playerLevel <= high then
    return 2 * (playerLevel - high) / (low - high), 1, 0
  elseif playerLevel <= high + 3 then
    local num = (playerLevel - high) / 6
    return num, 1 - num, num
  else
    return 0.5, 0.5, 0.5
  end
end

function pfUI.tourist.GetFactionColor(zone)
  if factions[zone] == (isHorde and "Alliance" or "Horde") then
    return 1, 0, 0
  elseif factions[zone] == (isHorde and "Horde" or "Alliance") then
    return 0, 1, 0
  else
    return 1, 1, 0
  end
end

local function retNil() return nil end
local function retOne(object, state)
  if state == object then
    return nil
  else
    return object
  end
end

local function retNormal(t, position)
  return (next(t, position))
end

function pfUI.tourist.IterateZoneInstances(zone)
  local inst = instances[zone]
  
  if not inst then
    return retNil
  elseif type(inst) == "table" then
    return retNormal, inst, nil
  else
    return retOne, inst, nil
  end
end

function pfUI.tourist.GetInstanceZone(instance)
  for k, v in pairs(instances) do
    if v then
      if type(v) == "string" then
        if v == instance then
          return k
        end
      else -- table
        for l in pairs(v) do
          if l == instance then
            return k
          end
        end
      end
    end
  end
end

function pfUI.tourist.DoesZoneHaveInstances(zone)
  return instances[zone] and true or false
end

local zonesInstances
local function initZonesInstances()
  if not zonesInstances then
    zonesInstances = {}
    for zone, v in pairs(lows) do
      if types[zone] ~= "Transport" then
        zonesInstances[zone] = true
      end
    end
  end
  initZonesInstances = nil
end

function pfUI.tourist.IterateZonesAndInstances()
  if initZonesInstances then
    initZonesInstances()
  end
  return retNormal, zonesInstances, nil
end

local function zoneIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and (types[k] == "Instance" or types[k] == "Battleground") do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateZones()
  if initZonesInstances then
    initZonesInstances()
  end
  return zoneIter, nil, nil
end

local function instanceIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and (types[k] ~= "Instance" or types[k] ~= "Battleground") do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist:IterateInstances()
  if initZonesInstances then
    initZonesInstances()
  end
  return instanceIter, nil, nil
end

local function bgIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and types[k] ~= "Battleground" do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateBattlegrounds()
  if initZonesInstances then
    initZonesInstances()
  end
  return bgIter, nil, nil
end

local function allianceIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and factions[k] ~= "Alliance" do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateAlliance()
  if initZonesInstances then
    initZonesInstances()
  end
  return allianceIter, nil, nil
end

local function hordeIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and factions[k] ~= "Horde" do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateHorde()
  if initZonesInstances then
    initZonesInstances()
  end
  return hordeIter, nil, nil
end

if isHorde then
  pfUI.tourist.IterateFriendly = pfUI.tourist.IterateHorde
  pfUI.tourist.IterateHostile = pfUI.tourist.IterateAlliance
else
  pfUI.tourist.IterateFriendly = pfUI.tourist.IterateAlliance
  pfUI.tourist.IterateHostile = pfUI.tourist.IterateHorde
end

local function contestedIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and factions[k] do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateContested()
  if initZonesInstances then
    initZonesInstances()
  end
  return contestedIter, nil, nil
end

local function kalimdorIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and continents[k] ~= Kalimdor do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateKalimdor()
  if initZonesInstances then
    initZonesInstances()
  end
  return kalimdorIter, nil, nil
end

local function easternKingdomsIter(_, position)
  local k = next(zonesInstances, position)
  while k ~= nil and continents[k] ~= Eastern_Kingdoms do
    k = next(zonesInstances, k)
  end
  return k
end
function pfUI.tourist.IterateEasternKingdoms()
  if initZonesInstances then
    initZonesInstances()
  end
  return easternKingdomsIter, nil, nil
end

function pfUI.tourist.IterateRecommendedZones()
  return retNormal, recZones, nil
end

function pfUI.tourist.IterateRecommendedInstances()
  return retNormal, recInstances, nil
end

function pfUI.tourist.HasRecommendedInstances()
  return next(recInstances) ~= nil
end

function pfUI.tourist.IsInstance(zone)
  local t = types[zone]
  return t == "Instance" or t == "Battleground"
end

function pfUI.tourist.IsZone(zone)
  local t = types[zone]
  return t ~= "Instance" and t ~= "Battleground" and t ~= "Transport"
end

function pfUI.tourist.IsZoneOrInstance(zone)
  local t = types[zone]
  return t and t ~= "Transport"
end

function pfUI.tourist.IsBattleground(zone)
  local t = types[zone]
  return t == "Battleground"
end

function pfUI.tourist.IsAlliance(zone)
  return factions[zone] == "Alliance"
end

function pfUI.tourist.IsHorde(zone)
  return factions[zone] == "Horde"
end

if isHorde then
  pfUI.tourist.IsFriendly = pfUI.tourist.IsHorde
  pfUI.tourist.IsHostile = pfUI.tourist.IsAlliance
else
  pfUI.tourist.IsFriendly = pfUI.tourist.IsAlliance
  pfUI.tourist.IsHostile = pfUI.tourist.IsHorde
end

function pfUI.tourist.IsContested(zone)
  return not factions[zone]
end

function pfUI.tourist.GetContinent(zone)
  return continents[zone] or UNKNOWN
end

function pfUI.tourist.IsInKalimdor(zone)
  return continents[zone] == Kalimdor
end

function pfUI.tourist.IsInEasternKingdoms(zone)
  return continents[zone] == Eastern_Kingdoms
end

function pfUI.tourist.GetInstanceGroupSize(instance)
  return groupSizes[instance] or 0
end

local inf = 1/0
local stack = setmetatable({}, {__mode='k'})
local function iterator(S)
  local position = S['#'] - 1
  S['#'] = position
  local x = S[position]
  if not x then
    for k in pairs(S) do
      S[k] = nil
    end
    stack[S] = true
    return nil
  end
  return x
end

setmetatable(cost, {
  __index = function(self, vertex)
    local price = 1
    
    if lows[vertex] > playerLevel then
      price = price * (1 + math.ceil((lows[vertex] - playerLevel) / 6))
    end
    
    if factions[vertex] == (isHorde and "Horde" or "Alliance") then
      price = price / 2
    elseif factions[vertex] == (isHorde and "Alliance" or "Horde") then
      if types[vertex] == "City" then
        price = price * 10
      else
        price = price * 3
      end
    end
    
    if types[x] == "Transport" then
      price = price * 2
    end
    
    self[vertex] = price
    return price
  end
})

function pfUI.tourist.IteratePath(alpha, bravo)
  if paths[alpha] == nil or paths[bravo] == nil then
    return retNil
  end
  
  local d = next(stack) or {}
  stack[d] = nil
  local Q = next(stack) or {}
  stack[Q] = nil
  local S = next(stack) or {}
  stack[S] = nil
  local pi = next(stack) or {}
  stack[pi] = nil
  
  for vertex, v in pairs(paths) do
    d[vertex] = inf
    Q[vertex] = v
  end
  d[alpha] = 0
  
  while next(Q) do
    local u
    local min = inf
    for z in pairs(Q) do
      local value = d[z]
      if value < min then
        min = value
        u = z
      end
    end
    if min == inf then
      return retNil
    end
    Q[u] = nil
    if u == bravo then
      break
    end
    
    local adj = paths[u]
    if type(adj) == "table" then
      local d_u = d[u]
      for v in pairs(adj) do
        local c = d_u + cost[v]
        if d[v] > c then
          d[v] = c
          pi[v] = u
        end
      end
    elseif adj ~= false then
      local c = d[u] + cost[adj]
      if d[adj] > c then
        d[adj] = c
        pi[adj] = u
      end
    end
  end
  
  local i = 1
  local last = bravo
  while last do
    S[i] = last
    i = i + 1
    last = pi[last]
  end
  
  for k in pairs(pi) do
    pi[k] = nil
  end
  for k in pairs(Q) do
    Q[k] = nil
  end
  for k in pairs(d) do
    d[k] = nil
  end
  stack[pi] = true
  stack[Q] = true
  stack[d] = true
  
  S['#'] = i
  
  return iterator, S
end

function pfUI.tourist.IterateBorderZones(zone)
  local path = paths[zone]
  if not path then
    return retNil
  elseif type(path) == "table" then
    return retNormal, path
  else
    return retOne, path
  end
end

function pfUI.tourist.enabled()
  pfUI.tourist.frame = CreateFrame("Frame", "TouristLibFrame", UIParent)
  pfUI.tourist.frame:UnregisterAllEvents()
  pfUI.tourist.frame:RegisterEvent("PLAYER_LEVEL_UP")
  pfUI.tourist.frame:RegisterEvent("PLAYER_ENTERING_WORLD")
  pfUI.tourist.frame:SetScript("OnEvent", function()
    PLAYER_LEVEL_UP()
  end)
  
  local BOOTYBAY_RATCHET_BOAT = string.format(X_Y_BOAT, "Booty Bay", "Ratchet")
  local MENETHIL_THERAMORE_BOAT = string.format(X_Y_BOAT, "Menethil Harbor", "Theramore Isle")
  local MENETHIL_AUBERDINE_BOAT = string.format(X_Y_BOAT, "Menethil Harbor", "Auberdine")
  local AUBERDINE_DARNASSUS_BOAT = string.format(X_Y_BOAT, "Auberdine", "Darnassus")
  local ORGRIMMAR_UNDERCITY_ZEPPELIN = string.format(X_Y_ZEPPELIN, "Orgrimmar", "Undercity")
  local ORGRIMMAR_GROMGOL_ZEPPELIN = string.format(X_Y_ZEPPELIN, "Orgrimmar", "Grom'gol Base Camp")
  local UNDERCITY_GROMGOL_ZEPPELIN = string.format(X_Y_ZEPPELIN, "Undercity", "Grom'gol Base Camp")

  local zones = {}
  
  zones[AUBERDINE_DARNASSUS_BOAT] = {
    paths = {
      ["Darkshore"] = true,
      ["Darnassus"] = true,
    },
    faction = "Alliance",
    type = "Transport",
  }

  zones[BOOTYBAY_RATCHET_BOAT] = {
    paths = {
      ["Stranglethorn Vale"] = true,
      ["The Barrens"] = true,
    },
    type = "Transport",
  }

  zones[MENETHIL_AUBERDINE_BOAT] = {
    paths = {
      ["Wetlands"] = true,
      ["Darkshore"] = true,
    },
    faction = "Alliance",
    type = "Transport",
  }

  zones[MENETHIL_THERAMORE_BOAT] = {
    paths = {
      ["Wetlands"] = true,
      ["Dustwallow Marsh"] = true,
    },
    faction = "Alliance",
    type = "Transport",
  }

  zones[ORGRIMMAR_GROMGOL_ZEPPELIN] = {
    paths = {
      ["Durotar"] = true,
      ["Stranglethorn Vale"] = true,
    },
    faction = "Horde",
    type = "Transport",
  }

  zones[ORGRIMMAR_UNDERCITY_ZEPPELIN] = {
    paths = {
      ["Durotar"] = true,
      ["Tirisfal Glades"] = true,
    },
    faction = "Horde",
    type = "Transport",
  }

  zones[UNDERCITY_GROMGOL_ZEPPELIN] = {
    paths = {
      ["Stranglethorn Vale"] = true,
      ["Tirisfal Glades"] = true,
    },
    faction = "Horde",
    type = "Transport",
  }

  zones["Alterac Valley"] = {
    continent = Eastern_Kingdoms,
    paths = "Alterac Mountains",
    groupSize = 40,
    type = "Battleground",
  }

  zones["Arathi Basin"] = {
    continent = Eastern_Kingdoms,
    paths = "Arathi Highlands",
    groupSize = 15,
    type = "Battleground",
  }

  zones["Warsong Gulch"] = {
    continent = Kalimdor,
    paths = isHorde and "The Barrens" or "Ashenvale",
    groupSize = 10,
    type = "Battleground",
  }

  zones["Deeprun Tram"] = {
    continent = Eastern_Kingdoms,
    paths = {
      ["Stormwind City"] = true,
      ["Ironforge"] = true,
    },
    faction = "Alliance",
  }

  zones["Ironforge"] = {
    continent = Eastern_Kingdoms,
    instances = "Gnomeregan",
    paths = {
      ["Dun Morogh"] = true,
      ["Deeprun Tram"] = true,
    },
    faction = "Alliance",
    type = "City",
  }
  
  zones["Stormwind City"] = {
    continent = Eastern_Kingdoms,
    instances = "The Stockade",
    paths = {
      ["Deeprun Tram"] = true,
      ["The Stockade"] = true,
      ["Elwynn Forest"] = true,
    },
    faction = "Alliance",
    type = "City",
  }
  
  zones["Undercity"] = {
    continent = Eastern_Kingdoms,
    instances = "Scarlet Monastery",
    paths = {
      ["Tirisfal Glades"] = true,
    },
    faction = "Horde",
    type = "City",
  }
  
  zones["Dun Morogh"] = {
    low = 1,
    high = 10,
    continent = Eastern_Kingdoms,
    instances = "Gnomeregan",
    paths = {
      ["Wetlands"] = true,
      ["Gnomeregan"] = true,
      ["Ironforge"] = true,
      ["Loch Modan"] = true,
    },
    faction = "Alliance",
  }
  
  zones["Elwynn Forest"] = {
    low = 1,
    high = 10,
    continent = Eastern_Kingdoms,
    instances = "The Stockade",
    paths = {
      ["Westfall"] = true,
      ["Redridge Mountains"] = true,
      ["Stormwind City"] = true,
      ["Duskwood"] = true,
    },
    faction = "Alliance",
  }
  
  zones["Tirisfal Glades"] = {
    low = 1,
    high = 10,
    continent = Eastern_Kingdoms,
    instances = "Scarlet Monastery",
    paths = {
      ["Western Plaguelands"] = true,
      ["Undercity"] = true,
      ["Scarlet Monastery"] = true,
      [UNDERCITY_GROMGOL_ZEPPELIN] = true,
      [ORGRIMMAR_UNDERCITY_ZEPPELIN] = true,
      ["Silverpine Forest"] = true,
    },
    faction = "Horde",
  }
  
  zones["Loch Modan"] = {
    low = 10,
    high = 20,
    continent = Eastern_Kingdoms,
    paths = {
      ["Wetlands"] = true,
      ["Badlands"] = true,
      ["Dun Morogh"] = true,
      ["Searing Gorge"] = not isHorde and true or nil,
    },
    faction = "Alliance",
  }

  zones["Silverpine Forest"] = {
    low = 10,
    high = 20,
    continent = Eastern_Kingdoms,
    instances = "Shadowfang Keep",
    paths = {
      ["Tirisfal Glades"] = true,
      ["Hillsbrad Foothills"] = true,
      ["Shadowfang Keep"] = true,
    },
    faction = "Horde",
  }

  zones["Westfall"] = {
    low = 10,
    high = 20,
    continent = Eastern_Kingdoms,
    instances = "The Deadmines",
    paths = {
      ["Duskwood"] = true,
      ["Elwynn Forest"] = true,
      ["The Deadmines"] = true,
    },
    faction = "Alliance",
  }

  zones["Redridge Mountains"] = {
    low = 15,
    high = 25,
    continent = Eastern_Kingdoms,
    paths = {
      ["Burning Steppes"] = true,
      ["Elwynn Forest"] = true,
      ["Duskwood"] = true,
    },
  }

  zones["Duskwood"] = {
    low = 18,
    high = 30,
    continent = Eastern_Kingdoms,
    paths = {
      ["Redridge Mountains"] = true,
      ["Stranglethorn Vale"] = true,
      ["Westfall"] = true,
      ["Deadwind Pass"] = true,
      ["Elwynn Forest"] = true,
    },
  }

  zones["Hillsbrad Foothills"] = {
    low = 20,
    high = 30,
    continent = Eastern_Kingdoms,
    paths = {
      ["Alterac Mountains"] = true,
      ["The Hinterlands"] = true,
      ["Arathi Highlands"] = true,
      ["Silverpine Forest"] = true,
    },
  }

  zones["Wetlands"] = {
    low = 20,
    high = 30,
    continent = Eastern_Kingdoms,
    paths = {
      ["Arathi Highlands"] = true,
      [MENETHIL_AUBERDINE_BOAT] = true,
      [MENETHIL_THERAMORE_BOAT] = true,
      ["Dun Morogh"] = true,
      ["Loch Modan"] = true,
    },
  }

  zones["Alterac Mountains"] = {
    low = 30,
    high = 40,
    continent = Eastern_Kingdoms,
    instances = "Alterac Valley",
    paths = {
      ["Western Plaguelands"] = true,
      ["Alterac Valley"] = true,
      ["Hillsbrad Foothills"] = true,
    },
  }

  zones["Arathi Highlands"] = {
    low = 30,
    high = 40,
    continent = Eastern_Kingdoms,
    instances = "Arathi Basin",
    paths = {
      ["Wetlands"] = true,
      ["Hillsbrad Foothills"] = true,
      ["Arathi Basin"] = true,
    },
  }

  zones["Stranglethorn Vale"] = {
    low = 30,
    high = 45,
    continent = Eastern_Kingdoms,
    instances = "Zul'Gurub",
    paths = {
      ["Zul'Gurub"] = true,
      [BOOTYBAY_RATCHET_BOAT] = true,
      ["Duskwood"] = true,
      [ORGRIMMAR_GROMGOL_ZEPPELIN] = true,
      [UNDERCITY_GROMGOL_ZEPPELIN] = true,
    },
  }

  zones["Badlands"] = {
    low = 35,
    high = 45,
    continent = Eastern_Kingdoms,
    instances = "Uldaman",
    paths = {
      ["Uldaman"] = true,
      ["Searing Gorge"] = true,
      ["Loch Modan"] = true,
    },
  }

  zones["Swamp of Sorrows"] = {
    low = 35,
    high = 45,
    continent = Eastern_Kingdoms,
    instances = "The Temple of Atal'Hakkar",
    paths = {
      ["Blasted Lands"] = true,
      ["Deadwind Pass"] = true,
      ["The Temple of Atal'Hakkar"] = true,
    },
  }

  zones["The Hinterlands"] = {
    low = 40,
    high = 50,
    continent = Eastern_Kingdoms,
    paths = {
      ["Hillsbrad Foothills"] = true,
      ["Western Plaguelands"] = true,
    },
  }

  zones["Searing Gorge"] = {
    low = 43,
    high = 50,
    continent = Eastern_Kingdoms,
    instances = {
      ["Blackrock Depths"] = true,
      ["Blackwing Lair"] = true,
      ["Molten Core"] = true,
      ["Blackrock Spire"] = true,
    },
    paths = {
      ["Blackrock Mountain"] = true,
      ["Badlands"] = true,
      ["Loch Modan"] = not isHorde and true or nil,
    },
  }

  zones["Blackrock Mountain"] = {
    low = 42,
    high = 54,
    continent = Eastern_Kingdoms,
    instances = {
      ["Blackrock Depths"] = true,
      ["Blackwing Lair"] = true,
      ["Molten Core"] = true,
      ["Blackrock Spire"] = true,
    },
    paths = {
      ["Burning Steppes"] = true,
      ["Blackwing Lair"] = true,
      ["Molten Core"] = true,
      ["Blackrock Depths"] = true,
      ["Searing Gorge"] = true,
      ["Blackrock Spire"] = true,
    },
  }

  zones["Deadwind Pass"] = {
    low = 55,
    high = 60,
    continent = Eastern_Kingdoms,
    paths = {
      ["Duskwood"] = true,
      ["Swamp of Sorrows"] = true,
    },
  }

  zones["Blasted Lands"] = {
    low = 45,
    high = 55,
    continent = Eastern_Kingdoms,
    paths = {
      ["Swamp of Sorrows"] = true,
    },
  }

  zones["Burning Steppes"] = {
    low = 50,
    high = 58,
    continent = Eastern_Kingdoms,
    instances = {
      ["Blackrock Depths"] = true,
      ["Blackwing Lair"] = true,
      ["Molten Core"] = true,
      ["Blackrock Spire"] = true,
    },
    paths = {
      ["Blackrock Mountain"] = true,
      ["Redridge Mountains"] = true,
    },
  }

  zones["Western Plaguelands"] = {
    low = 51,
    high = 58,
    continent = Eastern_Kingdoms,
    instances = "Scholomance",
    paths = {
      ["The Hinterlands"] = true,
      ["Eastern Plaguelands"] = true,
      ["Tirisfal Glades"] = true,
      ["Scholomance"] = true,
      ["Alterac Mountains"] = true,
    },
  }

  zones["Eastern Plaguelands"] = {
    low = 53,
    high = 60,
    continent = Eastern_Kingdoms,
    instances = {
      ["Stratholme"] = true,
      ["Naxxramas"] = true,
    },
    paths = {
      ["Western Plaguelands"] = true,
      ["Naxxramas"] = true,
      ["Stratholme"] = true,
    },
  }

  zones["The Deadmines"] = {
    low = isWestern and 17 or 15,
    high = isWestern and 26 or 20,
    continent = Eastern_Kingdoms,
    paths = "Westfall",
    groupSize = 5,
    faction = "Alliance",
    type = "Instance",
  }

  zones["Shadowfang Keep"] = {
    low = isWestern and 22 or 18,
    high = isWestern and 30 or 25,
    continent = Eastern_Kingdoms,
    paths = "Silverpine Forest",
    groupSize = 5,
    faction = "Horde",
    type = "Instance",
  }

  zones["The Stockade"] = {
    low = isWestern and 24 or 23,
    high = isWestern and 32 or 26,
    continent = Eastern_Kingdoms,
    paths = "Stormwind City",
    groupSize = 5,
    faction = "Alliance",
    type = "Instance",
  }

  zones["Gnomeregan"] = {
    low = isWestern and 29 or 24,
    high = isWestern and 38 or 33,
    continent = Eastern_Kingdoms,
    paths = "Dun Morogh",
    groupSize = 5,
    faction = "Alliance",
    type = "Instance",
  }

  zones["Scarlet Monastery"] = {
    low = isWestern and 34 or 30,
    high = isWestern and 45 or 40,
    continent = Eastern_Kingdoms,
    paths = "Tirisfal Glades",
    groupSize = 5,
    faction = "Horde",
    type = "Instance",
  }

  zones["Uldaman"] = {
    low = isWestern and 41 or 35,
    high = isWestern and 51 or 45,
    continent = Eastern_Kingdoms,
    paths = "Badlands",
    groupSize = 5,
    type = "Instance",
  }

  zones["The Temple of Atal'Hakkar"] = {
    low = isWestern and 50 or 44,
    high = isWestern and 60 or 50,
    continent = Eastern_Kingdoms,
    paths = "Swamp of Sorrows",
    groupSize = 5,
    type = "Instance",
  }

  zones["Blackrock Depths"] = {
    low = isWestern and 52 or 48,
    high = isWestern and 60 or 56,
    continent = Eastern_Kingdoms,
    paths = {
      ["Molten Core"] = true,
      ["Blackrock Mountain"] = true,
    },
    groupSize = 5,
    type = "Instance",
  }

  zones["Blackrock Spire"] = {
    low = isWestern and 55 or 53,
    high = 60,
    continent = Eastern_Kingdoms,
    paths = {
      ["Blackrock Mountain"] = true,
      ["Blackwing Lair"] = true,
    },
    groupSize = 10,
    type = "Instance",
  }

  zones["Scholomance"] = {
    low = 58,
    high = 60,
    continent = Eastern_Kingdoms,
    paths = "Western Plaguelands",
    groupSize = 5,
    type = "Instance",
  }

  zones["Stratholme"] = {
    low = isWestern and 58 or 55,
    high = 60,
    continent = Eastern_Kingdoms,
    paths = "Eastern Plaguelands",
    groupSize = 5,
    type = "Instance",
  }

  zones["Blackwing Lair"] = {
    low = 60,
    high = 62,
    continent = Eastern_Kingdoms,
    paths = "Blackrock Mountain",
    groupSize = 40,
    type = "Instance",
  }

  zones["Molten Core"] = {
    low = 60,
    high = 62,
    continent = Eastern_Kingdoms,
    paths = "Blackrock Mountain",
    groupSize = 40,
    type = "Instance",
  }

  zones["Zul'Gurub"] = {
    low = 60,
    high = 62,
    continent = Eastern_Kingdoms,
    paths = "Stranglethorn Vale",
    groupSize = 20,
    type = "Instance",
  }

  zones["Naxxramas"] = {
    low = 60,
    high = 70,
    continent = Eastern_Kingdoms,
    groupSize = 40,
    type = "Instance",
  }

  zones["Darnassus"] = {
    continent = Kalimdor,
    paths = {
      ["Teldrassil"] = true,
      [AUBERDINE_DARNASSUS_BOAT] = true,
    },
    faction = "Alliance",
    type = "City",
  }

  zones["Hyjal"] = {
    continent = Kalimdor,
  }

  zones["Moonglade"] = {
    continent = Kalimdor,
    paths = {
      ["Felwood"] = true,
      ["Winterspring"] = true,
    },
  }

  zones["Orgrimmar"] = {
    continent = Kalimdor,
    instances = "Ragefire Chasm",
    paths = {
      ["Durotar"] = true,
      ["Ragefire Chasm"] = true,
    },
    faction = "Horde",
    type = "City",
  }

  zones["Thunder Bluff"] = {
    continent = Kalimdor,
    paths = "Mulgore",
    faction = "Horde",
    type = "City",
  }

  zones["Durotar"] = {
    low = 1,
    high = 10,
    continent = Kalimdor,
    instances = "Ragefire Chasm",
    paths = {
      [ORGRIMMAR_UNDERCITY_ZEPPELIN] = true,
      [ORGRIMMAR_GROMGOL_ZEPPELIN] = true,
      ["The Barrens"] = true,
      ["Orgrimmar"] = true,
    },
    faction = "Horde",
  }

  zones["Mulgore"] = {
    low = 1,
    high = 10,
    continent = Kalimdor,
    paths = {
      ["Thunder Bluff"] = true,
      ["The Barrens"] = true,
    },
    faction = "Horde",
  }

  zones["Teldrassil"] = {
    low = 1,
    high = 10,
    continent = Kalimdor,
    paths = "Darnassus",
    faction = "Alliance",
  }

  zones["Darkshore"] = {
    low = 10,
    high = 20,
    continent = Kalimdor,
    paths = {
      [MENETHIL_AUBERDINE_BOAT] = true,
      [AUBERDINE_DARNASSUS_BOAT] = true,
      ["Ashenvale"] = true,
    },
    faction = "Alliance",
  }

  zones["The Barrens"] = {
    low = 10,
    high = 25,
    continent = Kalimdor,
    instances = {
      ["Razorfen Kraul"] = true,
      ["Wailing Caverns"] = true,
      ["Razorfen Downs"] = true,
      ["Warsong Gulch"] = isHorde and true or nil,
    },
    paths = {
      ["Thousand Needles"] = true,
      ["Razorfen Kraul"] = true,
      ["Ashenvale"] = true,
      ["Durotar"] = true,
      ["Wailing Caverns"] = true,
      [BOOTYBAY_RATCHET_BOAT] = true,
      ["Dustwallow Marsh"] = true,
      ["Razorfen Downs"] = true,
      ["Stonetalon Mountains"] = true,
      ["Mulgore"] = true,
      ["Warsong Gulch"] = isHorde and true or nil,
    },
    faction = "Horde",
  }

  zones["Stonetalon Mountains"] = {
    low = 15,
    high = 27,
    continent = Kalimdor,
    paths = {
      ["Desolace"] = true,
      ["The Barrens"] = true,
      ["Ashenvale"] = true,
    },
  }

  zones["Ashenvale"] = {
    low = 18,
    high = 30,
    continent = Kalimdor,
    instances = {
      ["Blackfathom Deeps"] = true,
      ["Warsong Gulch"] = not isHorde and true or nil,
    },
    paths = {
      ["Azshara"] = true,
      ["The Barrens"] = true,
      ["Blackfathom Deeps"] = true,
      ["Warsong Gulch"] = not isHorde and true or nil,
      ["Felwood"] = true,
      ["Darkshore"] = true,
      ["Stonetalon Mountains"] = true,
    },
  }

  zones["Thousand Needles"] = {
    low = 25,
    high = 35,
    continent = Kalimdor,
    paths = {
      ["Feralas"] = true,
      ["The Barrens"] = true,
      ["Tanaris"] = true,
    },
  }

  zones["Desolace"] = {
    low = 30,
    high = 40,
    continent = Kalimdor,
    instances = "Maraudon",
    paths = {
      ["Feralas"] = true,
      ["Stonetalon Mountains"] = true,
      ["Maraudon"] = true,
    },
  }

  zones["Dustwallow Marsh"] = {
    low = 35,
    high = 45,
    continent = Kalimdor,
    instances = "Onyxia's Lair",
    paths = {
      ["Onyxia's Lair"] = true,
      ["The Barrens"] = true,
      [MENETHIL_THERAMORE_BOAT] = true,
    },
  }

  zones["Feralas"] = {
    low = 40,
    high = 50,
    continent = Kalimdor,
    instances = "Dire Maul",
    paths = {
      ["Thousand Needles"] = true,
      ["Desolace"] = true,
      ["Dire Maul"] = true,
    },
  }
  
  zones["Tanaris"] = {
    low = 40,
    high = 50,
    continent = Kalimdor,
    instances = {
      ["Zul'Farrak"] = true,
    },
    paths = {
      ["Thousand Needles"] = true,
      ["Un'Goro Crater"] = true,
      ["Zul'Farrak"] = true,
    },
  }

  zones["Azshara"] = {
    low = 45,
    high = 55,
    continent = Kalimdor,
    paths = "Ashenvale",
  }

  zones["Felwood"] = {
    low = 48,
    high = 55,
    continent = Kalimdor,
    paths = {
      ["Winterspring"] = true,
      ["Moonglade"] = true,
      ["Ashenvale"] = true,
    },
  }

  zones["Un'Goro Crater"] = {
    low = 48,
    high = 55,
    continent = Kalimdor,
    paths = {
      ["Silithus"] = true,
      ["Tanaris"] = true,
    },
  }

  zones["Silithus"] = {
    low = 55,
    high = 60,
    continent = Kalimdor,
    instances = {
      ["Ahn'Qiraj"] = true,
      ["Ruins of Ahn'Qiraj"] = true,
    },
    paths = {
      ["Ruins of Ahn'Qiraj"] = true,
      ["Un'Goro Crater"] = true,
      ["Ahn'Qiraj"] = true,
    },
  }

  zones["Winterspring"] = {
    low = 55,
    high = 60,
    continent = Kalimdor,
    paths = {
      ["Felwood"] = true,
      ["Moonglade"] = true,
    },
  }

  zones["Ragefire Chasm"] = {
    low = 13,
    high = isWestern and 18 or 15,
    continent = Kalimdor,
    paths = "Orgrimmar",
    groupSize = 5,
    faction = "Horde",
    type = "Instance",
  }

  zones["Wailing Caverns"] = {
    low = isWestern and 17 or 15,
    high = isWestern and 24 or 21,
    continent = Kalimdor,
    paths = "The Barrens",
    groupSize = 5,
    faction = "Horde",
    type = "Instance",
  }

  zones["Blackfathom Deeps"] = {
    low = isWestern and 24 or 20,
    high = isWestern and 32 or 27,
    continent = Kalimdor,
    paths = "Ashenvale",
    groupSize = 5,
    type = "Instance",
  }

  zones["Razorfen Kraul"] = {
    low = isWestern and 29 or 25,
    high = isWestern and 38 or 35,
    continent = Kalimdor,
    paths = "The Barrens",
    groupSize = 5,
    type = "Instance",
  }

  zones["Razorfen Downs"] = {
    low = isWestern and 37 or 35,
    high = isWestern and 46 or 40,
    continent = Kalimdor,
    paths = "The Barrens",
    groupSize = 5,
    type = "Instance",
  }

  zones["Zul'Farrak"] = {
    low = isWestern and 44 or 43,
    high = isWestern and 54 or 47,
    continent = Kalimdor,
    paths = "Tanaris",
    groupSize = 5,
    type = "Instance",
  }

  zones["Maraudon"] = {
    low = isWestern and 46 or 40,
    high = isWestern and 55 or 49,
    continent = Kalimdor,
    paths = "Desolace",
    groupSize = 5,
    type = "Instance",
  }

  zones["Dire Maul"] = {
    low = 56,
    high = 60,
    continent = Kalimdor,
    paths = "Feralas",
    groupSize = 5,
    type = "Instance",
  }

  zones["Onyxia's Lair"] = {
    low = 60,
    high = 62,
    continent = Kalimdor,
    paths = "Dustwallow Marsh",
    groupSize = 40,
    type = "Instance",
  }

  zones["Ahn'Qiraj"] = {
    low = 60,
    high = 65,
    continent = Kalimdor,
    paths = "Silithus",
    groupSize = 40,
    type = "Instance",
  }

  zones["Ruins of Ahn'Qiraj"] = {
    low = 60,
    high = 65,
    continent = Kalimdor,
    paths = "Silithus",
    groupSize = 20,
    type = "Instance",
  }
  
  for k,v in pairs(zones) do
    lows[k] = v.low or 0
    highs[k] = v.high or 0
    continents[k] = v.continent or UNKNOWN
    instances[k] = v.instances
    paths[k] = v.paths or false
    types[k] = v.type or "Zone"
    groupSizes[k] = v.groupSize
    factions[k] = v.faction
  end
  
  PLAYER_LEVEL_UP()
end
