pfUI:RegisterModule("zoneinfo", function ()
  -- do not load if other map addon is loaded
  if Cartographer then return end
  if METAMAP_TITLE then return end

  pfUI.zoneinfo = {}
  pfUI.zoneinfo.lastZone = nil
  pfUI.zoneinfo.t = {}

  if not pfUI.zoneinfo.info then
  pfUI.tourist.enabled()
  pfUI.zoneinfo.info = CreateFrame("Frame", "MapZoneInfo", WorldMapFrame)
  pfUI.zoneinfo.info.text = WorldMapFrameAreaFrame:CreateFontString(nil, "OVERLAY", "GameFontHighlightLarge")

  local text = pfUI.zoneinfo.info.text
  local font, size = GameFontHighlightLarge:GetFont()
  text:SetFont(font, size, "OUTLINE")
  text:SetPoint("TOP", WorldMapFrameAreaDescription, "BOTTOM", 0, -5)
  text:SetWidth(1024)
  
  pfUI.zoneinfo.info:SetScript("OnUpdate", function()
    if not WorldMapDetailFrame:IsShown() or not WorldMapFrameAreaLabel:IsShown() then
      pfUI.zoneinfo.info.text:SetText("")
      pfUI.zoneinfo = nil
      return
    end
    local underAttack = false
    local zone = WorldMapFrameAreaLabel:GetText()
    if zone then
      zone = string.gsub(WorldMapFrameAreaLabel:GetText(), " |cff.+$", "")
      if WorldMapFrameAreaDescription:GetText() then
        underAttack = true
        zone = string.gsub(WorldMapFrameAreaDescription:GetText(), " |cff.+$", "")
      end
    end
    if GetCurrentMapContinent() == 0 then
      local c1, c2 = GetMapContinents()
      if zone == c1 or zone == c2 then
        WorldMapFrameAreaLabel:SetTextColor(1, 1, 1)
        pfUI.zoneinfo.info.text:SetText("")
        return
      end
    end
    if not zone or not pfUI.tourist.IsZoneOrInstance(zone) then
      zone = WorldMapFrame.areaName
    end
    WorldMapFrameAreaLabel:SetTextColor(1, 1, 1)
    if zone and (pfUI.tourist.IsZoneOrInstance(zone) or pfUI.tourist.DoesZoneHaveInstances(zone)) then
      if not underAttack then
        WorldMapFrameAreaLabel:SetTextColor(pfUI.tourist.GetFactionColor(zone))
        WorldMapFrameAreaDescription:SetTextColor(1, 1, 1)
      else
        WorldMapFrameAreaLabel:SetTextColor(1, 1, 1)
        WorldMapFrameAreaDescription:SetTextColor(pfUI.tourist.GetFactionColor(zone))
      end
      local low, high = pfUI.tourist.GetLevel(zone)
      if low > 0 and high > 0 then
        local r, g, b = pfUI.tourist.GetLevelColor(zone)
        local levelText
        if low == high then
          levelText = string.format(" |cff%02x%02x%02x[%d]|r", r * 255, g * 255, b * 255, high)
        else
          levelText = string.format(" |cff%02x%02x%02x[%d-%d]|r", r * 255, g * 255, b * 255, low, high)
        end
        local groupSize = pfUI.tourist.GetInstanceGroupSize(zone)
        local sizeText = ""
        if groupSize > 0 then
          sizeText = " " .. string.format("%d-man", groupSize)
        end
        if not underAttack then
          WorldMapFrameAreaLabel:SetText(string.gsub(WorldMapFrameAreaLabel:GetText(), " |cff.+$", "") .. levelText .. sizeText)
        else
          WorldMapFrameAreaDescription:SetText(string.gsub(WorldMapFrameAreaDescription:GetText(), " |cff.+$", "") .. levelText .. sizeText)
        end
      end
      if pfUI.tourist.DoesZoneHaveInstances(zone) then
        if pfUI.zoneinfo.lastZone ~= zone then
          pfUI.zoneinfo.lastZone = zone
          table.insert(pfUI.zoneinfo.t, string.format("|cffffff00%s:|r", "Instances"))
          for instance in pfUI.tourist.IterateZoneInstances(zone) do
            local low, high = pfUI.tourist.GetLevel(instance)
            local r1, g1, b1 = pfUI.tourist.GetFactionColor(instance)
            local r2, g2, b2 = pfUI.tourist.GetLevelColor(instance)
            local groupSize = pfUI.tourist.GetInstanceGroupSize(instance)
            if low == high then
              if groupSize > 0 then
                table.insert(pfUI.zoneinfo.t, string.format("|cff%02x%02x%02x%s|r |cff%02x%02x%02x[%d]|r " .. "%d-man", r1 * 255, g1 * 255, b1 * 255, instance, r2 * 255, g2 * 255, b2 * 255, high, groupSize))
              else
                table.insert(pfUI.zoneinfo.t, string.format("|cff%02x%02x%02x%s|r |cff%02x%02x%02x[%d]|r", r1 * 255, g1 * 255, b1 * 255, instance, r2 * 255, g2 * 255, b2 * 255, high))
              end
            else
              if groupSize > 0 then
                table.insert(pfUI.zoneinfo.t, string.format("|cff%02x%02x%02x%s|r |cff%02x%02x%02x[%d-%d]|r " .. "%d-man", r1 * 255, g1 * 255, b1 * 255, instance, r2 * 255, g2 * 255, b2 * 255, low, high, groupSize))
              else
                table.insert(pfUI.zoneinfo.t, string.format("|cff%02x%02x%02x%s|r |cff%02x%02x%02x[%d-%d]|r", r1 * 255, g1 * 255, b1 * 255, instance, r2 * 255, g2 * 255, b2 * 255, low, high))
              end
            end
          end
          pfUI.zoneinfo.info.text:SetText(table.concat(pfUI.zoneinfo.t, "\n"))
          for k in pairs(pfUI.zoneinfo.t) do
            pfUI.zoneinfo.t[k] = nil
          end
          table.setn(pfUI.zoneinfo.t, 0)
        end
      else
        pfUI.zoneinfo.lastZone = nil
        pfUI.zoneinfo.info.text:SetText("")
      end
    elseif not zone then
      pfUI.zoneinfo.lastZone = nil
      pfUI.zoneinfo.info.text:SetText("")
    end
  end)
  end
  pfUI.zoneinfo.info:Show()
end)
